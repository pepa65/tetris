# tetris

## Tetris on pygame

* Required: python2.7 pygame
* Install: `wget -q good4.eu/tetris; chmod +x tetris`
* Run: `./tetris` (or move the `tetris` file somewhere into your `PATH`)

## Paint on kivy

* Required: python2.7 kivy
* Install: `wget -q good4.eu/paint; chmod +x paint`
* Run: `./paint` (or move the `paint` file somewhere into your `PATH`)
